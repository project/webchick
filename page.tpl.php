<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>

<?php /* different ids allow for separate theming of the home page */ ?>
<body class="<?php print $body_classes; ?>">
<div id="header">
  <?php print $header; ?>
  <?php if ($logo): ?>
    <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>">
      <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo" />
    </a>
  <?php else: ?>  
    <?php if ($site_name): ?>
      <h1 id='site-name'>
        <a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>">
          <?php print $site_name; ?>
        </a>
      </h1>
    <?php endif; ?>    
    <?php if ($site_slogan): ?>
      <div id='site-slogan'>
        <?php print $site_slogan; ?>
      </div>
    <?php endif; ?>  
  <?php endif; ?>
</div>
<div id="topnav">
  <?php if ($primary_links): ?>
    <!-- TODO: Format the links correctly -->
    <?php print theme('links', $primary_links, ''); ?>
  <?php endif; ?>
</div> <!-- /topnav -->
<div id="contentarea">
  <div id="content">    
    <?php if ($breadcrumb): ?><div id="header-region"><?php print $breadcrumb; ?></div><?php endif; ?>
    <?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
    <?php if ($content_top):?><div id="content-top"><?php print $content_top; ?></div><?php endif; ?>
    <?php if ($title): ?><h1 class="title"><?php print $title; ?></h1><?php endif; ?>
    <?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
    <?php print $help; ?>
    <?php print $messages; ?>
    <?php print $content; ?>
    <?php print $feed_icons; ?>
    <?php if ($content_bottom): ?><div id="content-bottom"><?php print $content_bottom; ?></div><?php endif; ?>
  </div> <!-- /content -->
</div> <!-- /contentarea -->

<div id="botnav">
  <?php print theme('links', $primary_links, ' | '); ?>
</div>
<div id="copyright">
  <?php print $footer_message; ?>
</div>
<?php if ($footer):?><div id="footer"><?php print $footer; ?></div><?php endif; ?>
<?php print $closure; ?>
</body>
</html>
