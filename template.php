<?php
// $Id $

/**
 * @file
 * File which contains theme overrides for the webchick theme.
 */

/*
 * ABOUT
 *
 *  webchick's website in Drupal form.
 */

/**
  * Return a menu that outputs the correct HTML for webchick.net .
  *
  * @param $links
  *   An array containing the links.
  * @param $delimiter
  *   The text delimiter to use when outputing the links.
  * @return a string containing the menu output.
  */
function webchick_links(&$links, $delimiter = '') {

  if (!count($links)) {
    return '';
  }
  $level_tmp = explode('-', key($links));
  $level = $level_tmp[0];
  $output = 'hi';//"<ul class=\"links-$level\">\n";
  $num = count($links);
  foreach ($links as $index => $link) {
    $output .= '';//'<li';
    if (stristr($index, 'active') && $delimiter == '') { 
      $output .= "<span class=\"activelink\">{$link['title']}</span>";
    }
    else {
      if ($num == 1 && $delimiter == '') {
        $link['attributes'] = array('class' => 'last');
      }
      $output .= l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment']);
    }
    if ($num > 1) {
      $output .= $delimiter;
    }
    $num--;
  }
  return $output;
}
